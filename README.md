# Hashistack

## Description
This repository serves as the IAC (infrastructure as code) configuration for the Hamsterweel platform.
Hashistack is a collection of integrated Hashicorp tools including:
- Consul: 
- Nomad: 
- Vault:
- Packer: 

These tools will be used to configure and provision the distributed infrastructure for the Hamsterwheel platform.

## Repository Structure
From the root of the repository contains multiple folders corresponding to the configuration of each Hashicorp tool.

## Installation
This repository uses conventional commit configurations to lint commit messages. This needs to be installed before additional commits are made on the repository:

1. Install node & npm
2. Install dependencies:
    ```bash
    npm install
    ```
3. Install husky to prepare commit message linting
    ```bash
    npm install husky-install
    ```

## Execution
For more information on how to run most parts of the repository, see the [Wiki](https://gitlab.com/hamster_wheel/infrastructure/hashistack/-/wikis/home)