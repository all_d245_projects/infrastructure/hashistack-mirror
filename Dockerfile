FROM ubuntu:focal

LABEL maintainer="Aldrich Saltson (@all_d245)"

# Set environment variables
ENV PACKER_BUILD_DIR="/packer-build"
ENV PACKER_DIST_DIR="${PACKER_BUILD_DIR}/dist"

# Initialize packer build directory
COPY packer ${PACKER_BUILD_DIR}

# Copy entrypoint script
COPY docker-entrypoint.sh /

# Install sudo
RUN apt-get update -qq && \
    apt-get install -yqq sudo

# Set permissions
RUN chmod -R 755 ${PACKER_BUILD_DIR}/scripts && \
    chmod 755 /docker-entrypoint.sh

# Provision image with dependencies
RUN ${PACKER_BUILD_DIR}/scripts/provision-box.sh

# Set dist directory
VOLUME [ "${PACKER_DIST_DIR}" ]

# Set working directory
WORKDIR ${PACKER_BUILD_DIR}

# Run entrypoint file
ENTRYPOINT ["/docker-entrypoint.sh"]
