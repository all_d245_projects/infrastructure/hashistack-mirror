variable "tailscale" {
  type = object({
    execution_env = string
  })

  default = {
    execution_env = "dev"
  }
}

local "tailscale_execution_env" {
  expression = "${var.tailscale.execution_env}"
}
