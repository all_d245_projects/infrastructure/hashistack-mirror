source "null" "ansible_host" {
      communicator = "none"
}

build {
  sources = [
    "source.null.ansible_host"
  ]

  provisioner "ansible" {
    inventory_file       = "${path.root}/../inventory.${local.tailscale_execution_env}.yml"
    playbook_file        = "${path.root}/tailscale-playbook.yml"
    galaxy_file          = "${path.root}/roles/requirements.yml"
    galaxy_force_install = true
  }
}
