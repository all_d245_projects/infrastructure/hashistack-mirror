variable "vault" {
  type = object({
    execution_env = string
  })

  default = {
    execution_env = "dev"
  }
}

local "vault_execution_env" {
  expression = "${var.vault.execution_env}"
}
