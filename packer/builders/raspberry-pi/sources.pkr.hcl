source "arm-image" "arm64-raspberry-pi-ubuntu-server-20-04" {
  additional_chroot_mounts = [["bind", "/run/systemd", "/run/systemd"]]
  image_mounts             = ["/boot/firmware", "/"]
  image_type               = "raspberrypi"
  iso_url                  = "https://cdimage.ubuntu.com/releases/20.04/release/ubuntu-20.04.4-preinstalled-server-arm64+raspi.img.xz"
  iso_checksum             = "6aeba20c00ef13ee7b48c57217ad0d6fc3b127b3734c113981d9477aceb4dad7"
  output_filename          = "output/arm64-rpi-ubuntu-server-20-04.img"
  qemu_binary              = "qemu-aarch64-static"
}

source "arm-image" "armhf-raspberry-pi-ubuntu-server-20-04" {
  additional_chroot_mounts = [["bind", "/run/systemd", "/run/systemd"]]
  image_mounts             = ["/boot/firmware", "/"]
  image_type               = "raspberrypi"
  iso_url                  = "https://cdimage.ubuntu.com/releases/20.04/release/ubuntu-20.04.4-preinstalled-server-armhf+raspi.img.xz"
  iso_checksum             = "3b1704e8e4ff8e01dd89b9dd6adf9b99b48b2a7530d6f7676ce8c37772ff4178"
  output_filename          = "output/armhf-rpi-ubuntu-server-20-04.img"
}