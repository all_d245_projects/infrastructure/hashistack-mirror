packer {
  required_plugins {
    arm-image = {
      version = "= 0.2.6"
      source = "github.com/solo-io/arm-image"
    }
  }
}

build {
  sources = [
    "source.arm-image.arm64-raspberry-pi-ubuntu-server-20-04",
    "source.arm-image.armhf-raspberry-pi-ubuntu-server-20-04"
  ]

  post-processor "compress" {
    output = "${path.root}/../dist/${source.name}.img.xz"
  }
}
