sudo apt-get update -qq

# Install required packages
sudo apt-get install -y \
    kpartx \
    qemu-user-static \
    wget \
    curl \
    zip \
    unzip \
    xz-utils

# Download and install packer
wget https://releases.hashicorp.com/packer/1.8.3/packer_1.8.3_linux_amd64.zip -q -O /tmp/packer_1.8.3_linux_amd64.zip
unzip -u -d /tmp /tmp/packer_1.8.3_linux_amd64.zip
sudo cp /tmp/packer /usr/local/bin

#cleanup
rm -rf /tmp/packer*